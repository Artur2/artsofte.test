﻿using Artsofte.Test.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Artsofte.Test.Code.Extensions
{
    public static class ControllerExtensions
    {
        public static bool TryUpdateModel<T>(this Controller @controller, T model, DbContext context, FormCollection collection) where T : Entity
        {
            try
            {
                var entryType = model.GetType();
                var properties = entryType.GetProperties();

                foreach (var key in collection.Keys)
                {
                    var value = collection.GetValue((string)key).AttemptedValue;

                    var property = properties.FirstOrDefault(x => x.Name.Equals(key));
                    if (property == null)
                        continue;
                    var propertyType = property.PropertyType;

                    if ((propertyType.IsValueType || propertyType.FullName == typeof(string).FullName) &&
                        propertyType != typeof(Guid))
                    {
                        property.SetValue(model, Convert.ChangeType(value, propertyType));
                    }
                    else if (propertyType == typeof(Guid))
                    {
                        property.SetValue(model, Guid.Parse(value));
                    }
                    else if (propertyType.IsClass && typeof(Entity).IsAssignableFrom(propertyType))
                    {
                        var @object = context.Set(propertyType)
                            .Find(new Guid(value.ToString()));
                        if (@object != null)
                            property.SetValue(model, @object);
                    }
                    //TODO: нужно доработать для всех типов e.g. (Enums and other like Guid or unify)
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                @controller.ModelState.AddModelError("_FORM", ex);
                return false;
            }

            return true;
        }
    }
}