﻿using System.Web.Mvc;

namespace Artsofte.Test.Areas.Team
{
    public class TeamAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Team";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Team_default",
                "Team/{controller}/{action}/{id}",
                new { controller = "Employees", action = "List", id = UrlParameter.Optional }
            );
        }
    }
}