﻿using Artsofte.Test.Areas.Team.Models;
using Artsofte.Test.Models.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Artsofte.Test.Areas.Team.Controllers
{
    public class ProgrammingLanguagesController : Controller
    {
        // GET: Team/ProgrammingLanguages
        public ActionResult List()
        {
            var context = DataContext.JoinOrCreate();

            var languages = context.Languages
                .OrderBy(x => x.Title);

            return View(languages);
        }

        [HttpGet]
        public ActionResult Add()
        {
            var model = new ProgrammingLanguage()
            {
                Id = Guid.NewGuid()
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Add(FormCollection collection)
        {
            var context = DataContext.JoinOrCreate();
            var model = new ProgrammingLanguage();

            TryUpdateModel(model);
            if (!ModelState.IsValid)
                return View(model);

            context.Languages.Add(model);
            context.SaveChanges();

            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            var context = DataContext.JoinOrCreate();

            var model = context.Languages.Find(id);
            if (model == null)
                return HttpNotFound();

            return View(model);
        }

        public ActionResult Edit(Guid id, FormCollection collection)
        {
            var context = DataContext.JoinOrCreate();

            var model = context.Languages.Find(id);
            if (model == null)
                return HttpNotFound();

            TryUpdateModel(model, collection);

            if (!ModelState.IsValid)
                return View(model);


            context.SaveChanges();

            return RedirectToAction("List");
        }

        public ActionResult Delete(Guid id)
        {
            var context = DataContext.JoinOrCreate();

            var model = context.Languages.Find(id);
            if (model == null)
                return HttpNotFound();

            context.Languages.Remove(model);
            context.SaveChanges();

            return RedirectToAction("List");
        }
    }
}