﻿using Artsofte.Test.Models.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Artsofte.Test.Areas.Team.Controllers
{
    public class AjaxController : Controller
    {
        // GET: Team/Ajax
        public ActionResult Names(string term)
        {
            var context = DataContext.JoinOrCreate();

            var names = context.Names
                .Where(x => x.Title.Contains(term))
                .OrderBy(x => x.Title)
                .Select(x => x.Title);

            return Json(names, JsonRequestBehavior.AllowGet);
        }
    }
}