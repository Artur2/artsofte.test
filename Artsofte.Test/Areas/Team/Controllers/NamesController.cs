﻿using Artsofte.Test.Areas.Team.Models;
using Artsofte.Test.Models.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Artsofte.Test.Areas.Team.Controllers
{
    public class NamesController : Controller
    {
        public ActionResult List()
        {
            var context = DataContext.JoinOrCreate();

            var names = context.Names
                .OrderBy(x => x.Title);

            return View(names);
        }

        [HttpGet]
        public ActionResult Add()
        {
            var model = new Name()
            {
                Id = Guid.NewGuid()
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Add(FormCollection collection)
        {
            var context = DataContext.JoinOrCreate();
            var model = new Name();

            TryUpdateModel(model);
            if (!ModelState.IsValid)
                return View(model);

            context.Names.Add(model);
            context.SaveChanges();

            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            var context = DataContext.JoinOrCreate();

            var model = context.Names.Find(id);
            if (model == null)
                return HttpNotFound();

            return View(model);
        }

        public ActionResult Edit(Guid id, FormCollection collection)
        {
            var context = DataContext.JoinOrCreate();

            var model = context.Names.Find(id);
            if (model == null)
                return HttpNotFound();

            TryUpdateModel(model, collection);

            if (!ModelState.IsValid)
                return View(model);


            context.SaveChanges();

            return RedirectToAction("List");
        }

        public ActionResult Delete(Guid id)
        {
            var context = DataContext.JoinOrCreate();

            var model = context.Names.Find(id);
            if (model == null)
                return HttpNotFound();

            context.Names.Remove(model);
            context.SaveChanges();

            return RedirectToAction("List");
        }
    }
}