﻿using Artsofte.Test.Areas.Team.Models;
using Artsofte.Test.Models.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Artsofte.Test.Code.Extensions;
using System.Diagnostics;

namespace Artsofte.Test.Areas.Team.Controllers
{
    public class EmployeesController : Controller
    {
        public ActionResult List()
        {
            var context = DataContext.JoinOrCreate();

            var employees = context.Employees
                .OrderBy(x => x.Name);

            return View(employees);
        }

        [HttpGet]
        public ActionResult Add()
        {
            var employee = new Employee()
            {
                Id = Guid.NewGuid()
            };

            return View(employee);
        }

        [HttpPost]
        public ActionResult Add(FormCollection collection)
        {
            var context = DataContext.JoinOrCreate();
            var model = new Employee();

            TryUpdateModel(model, collection);

            if (!ModelState.IsValid)
                return View(model);

            context.Employees.Add(model);
            context.SaveChanges();

            return RedirectToAction("List");
        }

        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            var context = DataContext.JoinOrCreate();

            var model = context.Employees.Find(id);
            if (model == null)
                return HttpNotFound();

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Guid id, FormCollection collection)
        {
            var context = DataContext.JoinOrCreate();
            var model = context.Employees.Find(id);
            if (model == null)
                return HttpNotFound();

            TryUpdateModel(model, collection);

            if (!ModelState.IsValid)
            {
                context.Rollback();
                return View(model);
            }

            context.SaveChanges();

            return RedirectToAction("List");
        }

        public ActionResult Delete(Guid id)
        {
            var context = DataContext.JoinOrCreate();

            var model = context.Employees.Find(id);
            if (model == null)
                return HttpNotFound();

            context.Employees.Remove(model);
            context.SaveChanges();

            return RedirectToAction("List");
        }

        public ActionResult Detail(Guid id)
        {
            var context = DataContext.JoinOrCreate();

            var model = context.Employees.Find(id);
            if (model == null)
                return HttpNotFound();

            return View(model);
        }
    }
}