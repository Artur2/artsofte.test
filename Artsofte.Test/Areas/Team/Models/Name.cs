﻿using Artsofte.Test.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Artsofte.Test.Areas.Team.Models
{
    public class Name : Entity
    {
        [Display(Name = "Имя")]
        [Required]
        public  string Title { get; set; }
    }
}