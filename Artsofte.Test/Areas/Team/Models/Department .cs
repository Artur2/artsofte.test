﻿using Artsofte.Test.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Artsofte.Test.Areas.Team.Models
{
    public class Department : Entity
    {
        [Display(Name = "Название")]
        [Required]
        public string Title { get; set; }

        [Display(Name = "Этаж")]
        [Required]
        public int Floor { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }

        public override string ToString()
        {
            return string.Format("{0}={1}", Title, Id);
        }
    }
}