﻿using Artsofte.Test.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Artsofte.Test.Areas.Team.Models
{
    public class Employee : Entity
    {
        [Display(Name = "Имя")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Фамилия")]
        [Required]
        public string SecondName { get; set; }

        [Display(Name = "Возраст")]
        [Required]
        public int Age { get; set; }

        [Display(Name = "Пол")]
        [Required]
        public string Gender { get; set; }

        [Display(Name = "Департамент")]
        public virtual Department Department { get; set; }

        public Guid DepartmentId { get; set; }

        [Display(Name = "Язык программирования")]
        public virtual ProgrammingLanguage Language { get; set; }

        public Guid LanguageId { get; set; }


        public override string ToString()
        {
            return string.Format("{0}={1}", Name, Id);
        }
    }
}