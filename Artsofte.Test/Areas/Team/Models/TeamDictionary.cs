﻿using Artsofte.Test.Models;
using Artsofte.Test.Models.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Artsofte.Test.Areas.Team.Models
{
    public class TeamDictionary
    {
        public static SelectList Departments(object selected = null)
        {
            var context = DataContext.JoinOrCreate();
            var items = new List<SelectListItem>();

            foreach (var item in context.Departments.OrderBy(x => x.Title))
            {
                var selectItem = new SelectListItem()
                    {
                        Text = item.Title,
                        Value = item.Id.ToString()
                    };

                items.Add(selectItem);
            }

            if (selected is Entity)
                selected = (selected as Entity).Id;

            return new SelectList(items, "Value", "Text", selected);
        }

        public static SelectList ProgrammingLanguages(object selected = null)
        {
            var context = DataContext.JoinOrCreate();
            var items = new List<SelectListItem>();

            foreach (var item in context.Languages.OrderBy(x => x.Title))
            {
                var selectItem = new SelectListItem()
                {
                    Text = item.Title,
                    Value = item.Id.ToString()
                };
                items.Add(selectItem);
            }

            if (selected is Entity)
                selected = (selected as Entity).Id;

            return new SelectList(items, "Value", "Text", selected);
        }

        public static SelectList Gender(object selected = null)
        {
            var items = new List<SelectListItem>();

            items.Add(new SelectListItem
                {
                    Text = "муж",
                    Value = "муж"
                });

            items.Add(new SelectListItem
            {
                Text = "жен",
                Value = "жен"
            });

            return new SelectList(items, "Value", "Text", selected);
        }
    }
}