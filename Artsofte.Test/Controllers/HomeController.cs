﻿using Artsofte.Test.Models.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Artsofte.Test.Controllers
{
    public class HomeController : Controller
    {
        private DataContext context;

        public HomeController()
        {
            context = DataContext.JoinOrCreate();
        }

        public ActionResult Index()
        {
            var names = context.Names;
            Debug.WriteLine(names.Count());
            ViewBag.NamesCount = names.Count();
            return View(names);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}