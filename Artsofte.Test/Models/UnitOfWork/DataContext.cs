﻿using Artsofte.Test.Areas.Team.Models;
using Artsofte.Test.Models.Configurations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Threading;
using System.Web;

namespace Artsofte.Test.Models.UnitOfWork
{
    public class DataContext : DbContext
    {
        private int CreatedThreadId { get; set; }

        [ThreadStatic]
        private static DataContext context;

        public DataContext()
            : base("DefaultConnection")
        {
            CreatedThreadId = Thread.CurrentThread.ManagedThreadId;
        }

        public static DataContext JoinOrCreate()
        {
            if (context == null)
                context = new DataContext();

            context.EnsureThreadSafety();
            return context;
        }

        public static DataContext CreateContextless()
        {
            return context = new DataContext();
        }


        private void EnsureThreadSafety()
        {
            if (!CreatedThreadId.Equals(Thread.CurrentThread.ManagedThreadId))
                throw new InvalidOperationException("DbContext is not thread safe, use it only in single thread");
        }

        public static void Close()
        {
            if (context != null)
            {
                context.EnsureThreadSafety();
                context = null;
            }
        }

        private IDbSet<Employee> _employees;

        public IDbSet<Employee> Employees
        {
            get
            {
                EnsureThreadSafety();
                if (_employees == null)
                    _employees = Set<Employee>();

                return _employees;
            }
        }


        private IDbSet<Department> _departments;

        public IDbSet<Department> Departments
        {
            get
            {
                EnsureThreadSafety();
                if (_departments == null)
                    _departments = Set<Department>();

                return _departments;
            }
        }

        private IDbSet<ProgrammingLanguage> _languages;

        public IDbSet<ProgrammingLanguage> Languages
        {
            get
            {
                EnsureThreadSafety();
                if (_languages == null)
                    _languages = Set<ProgrammingLanguage>();

                return _languages;
            }
        }

        private IDbSet<Name> _names;

        public IDbSet<Name> Names
        {
            get
            {
                EnsureThreadSafety();
                if (_names == null)
                    _names = Set<Name>();

                return _names;
            }
        }

        public void Rollback()
        {
            //todo: Test
            foreach (var item in ChangeTracker.Entries())
            {
                if (item.State == EntityState.Added)
                    item.State = EntityState.Deleted;
                else
                    item.State = EntityState.Unchanged;

                item.CurrentValues.SetValues(item.OriginalValues);
            }
        }

        public void RefreshObjects()
        {
            foreach (var item in ChangeTracker.Entries())
            {
                item.CurrentValues.SetValues(item.OriginalValues);
            }
        }

        public void SaveAndRefresh()
        {
            SaveChanges();
            RefreshObjects();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //TODO: test configuration
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new ProgrammingLanguageEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new DepartmentEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new EmployeeEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new NameEntityTypeConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}