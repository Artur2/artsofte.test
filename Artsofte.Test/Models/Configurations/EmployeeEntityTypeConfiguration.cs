﻿using Artsofte.Test.Areas.Team.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Artsofte.Test.Models.Configurations
{
    public class EmployeeEntityTypeConfiguration : EntityTypeConfiguration<Employee>
    {
        public EmployeeEntityTypeConfiguration()
        {
            this.HasKey(x => x.Id);
            this.Property(x => x.Name).IsRequired();
            this.Property(x => x.SecondName).IsRequired();
            this.Property(x => x.Age).IsRequired();
            this.Property(x => x.DepartmentId).IsOptional();
            this.Property(x => x.LanguageId).IsOptional();
        }
    }
}