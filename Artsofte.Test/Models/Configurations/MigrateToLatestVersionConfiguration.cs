﻿using Artsofte.Test.Areas.Team.Models;
using Artsofte.Test.Models.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace Artsofte.Test.Models.Configurations
{
    public class MigrateToLatestVersionConfiguration : DbMigrationsConfiguration<DataContext>
    {
        public MigrateToLatestVersionConfiguration()
        {
            this.AutomaticMigrationsEnabled = true;
            this.AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(DataContext context)
        {
            #region Names

            context.Names.AddOrUpdate<Name>(x => x.Title,
            new Name { Id = Guid.NewGuid(), Title = "Август" },
            new Name { Id = Guid.NewGuid(), Title = "Авдей" },
            new Name { Id = Guid.NewGuid(), Title = "Аверкий" },
            new Name { Id = Guid.NewGuid(), Title = "Аверьян" },
            new Name { Id = Guid.NewGuid(), Title = "Авксентий" },
            new Name { Id = Guid.NewGuid(), Title = "Автоном" },
            new Name { Id = Guid.NewGuid(), Title = "Агап" },
            new Name { Id = Guid.NewGuid(), Title = "Агафон" },
            new Name { Id = Guid.NewGuid(), Title = "Аггей" },
            new Name { Id = Guid.NewGuid(), Title = "Адам" },
            new Name { Id = Guid.NewGuid(), Title = "Адриан" },
            new Name { Id = Guid.NewGuid(), Title = "Азарий" },
            new Name { Id = Guid.NewGuid(), Title = "Аким" },
            new Name { Id = Guid.NewGuid(), Title = "Александр" },
            new Name { Id = Guid.NewGuid(), Title = "Алексей" },
            new Name { Id = Guid.NewGuid(), Title = "Амвросий" },
            new Name { Id = Guid.NewGuid(), Title = "Амос" },
            new Name { Id = Guid.NewGuid(), Title = "Ананий" },
            new Name { Id = Guid.NewGuid(), Title = "Анатолий" },
            new Name { Id = Guid.NewGuid(), Title = "Андрей" },
            new Name { Id = Guid.NewGuid(), Title = "Андрон" },
            new Name { Id = Guid.NewGuid(), Title = "Андроник" },
            new Name { Id = Guid.NewGuid(), Title = "Аникей" },
            new Name { Id = Guid.NewGuid(), Title = "Аникита" },
            new Name { Id = Guid.NewGuid(), Title = "Анисим " },
            new Name { Id = Guid.NewGuid(), Title = "Антип" },
            new Name { Id = Guid.NewGuid(), Title = "Антонин" },
            new Name { Id = Guid.NewGuid(), Title = "Аполлинарий" },
            new Name { Id = Guid.NewGuid(), Title = "Аполлон" },
            new Name { Id = Guid.NewGuid(), Title = "Арефий" },
            new Name { Id = Guid.NewGuid(), Title = "Аристарх" },
            new Name { Id = Guid.NewGuid(), Title = "Аркадий" },
            new Name { Id = Guid.NewGuid(), Title = "Арсений" },
            new Name { Id = Guid.NewGuid(), Title = "Артемий" },
            new Name { Id = Guid.NewGuid(), Title = "Артем" },
            new Name { Id = Guid.NewGuid(), Title = "Архип" },
            new Name { Id = Guid.NewGuid(), Title = "Аскольд" },
            new Name { Id = Guid.NewGuid(), Title = "Афанасий" },
            new Name { Id = Guid.NewGuid(), Title = "Афиноген" },
            new Name { Id = Guid.NewGuid(), Title = "Бажен" },
            new Name { Id = Guid.NewGuid(), Title = "Богдан" },
            new Name { Id = Guid.NewGuid(), Title = "Болеслав" },
            new Name { Id = Guid.NewGuid(), Title = "Борис" },
            new Name { Id = Guid.NewGuid(), Title = "Борислав" },
            new Name { Id = Guid.NewGuid(), Title = "Боян" },
            new Name { Id = Guid.NewGuid(), Title = "Бронислав" },
            new Name { Id = Guid.NewGuid(), Title = "Будимир" },
            new Name { Id = Guid.NewGuid(), Title = "Вадим" },
            new Name { Id = Guid.NewGuid(), Title = "Валентин" },
            new Name { Id = Guid.NewGuid(), Title = "Валерий" },
            new Name { Id = Guid.NewGuid(), Title = "Валерьян" },
            new Name { Id = Guid.NewGuid(), Title = "Варлаам" },
            new Name { Id = Guid.NewGuid(), Title = "Варфоломей" },
            new Name { Id = Guid.NewGuid(), Title = "Василий" },
            new Name { Id = Guid.NewGuid(), Title = "Вацлав" },
            new Name { Id = Guid.NewGuid(), Title = "Велимир" },
            new Name { Id = Guid.NewGuid(), Title = "Венедикт" },
            new Name { Id = Guid.NewGuid(), Title = "Вениамин" },
            new Name { Id = Guid.NewGuid(), Title = "Викентий" },
            new Name { Id = Guid.NewGuid(), Title = "Виктор" },
            new Name { Id = Guid.NewGuid(), Title = "Викторин" },
            new Name { Id = Guid.NewGuid(), Title = "Виссарион" },
            new Name { Id = Guid.NewGuid(), Title = "Виталий" },
            new Name { Id = Guid.NewGuid(), Title = "Владилен" },
            new Name { Id = Guid.NewGuid(), Title = "Владлен" },
            new Name { Id = Guid.NewGuid(), Title = "Владимир" },
            new Name { Id = Guid.NewGuid(), Title = "Владислав" },
            new Name { Id = Guid.NewGuid(), Title = "Влас" },
            new Name { Id = Guid.NewGuid(), Title = "Всеволод" },
            new Name { Id = Guid.NewGuid(), Title = "Всемил" },
            new Name { Id = Guid.NewGuid(), Title = "Всеслав" },
            new Name { Id = Guid.NewGuid(), Title = "Вышеслав" },
            new Name { Id = Guid.NewGuid(), Title = "Вячеслав" },
            new Name { Id = Guid.NewGuid(), Title = "Гаврила" },
            new Name { Id = Guid.NewGuid(), Title = "Галактион" },
            new Name { Id = Guid.NewGuid(), Title = "Гедеон" },
            new Name { Id = Guid.NewGuid(), Title = "Геннадий" },
            new Name { Id = Guid.NewGuid(), Title = "Георгий" },
            new Name { Id = Guid.NewGuid(), Title = "Герасим" },
            new Name { Id = Guid.NewGuid(), Title = "Герман" },
            new Name { Id = Guid.NewGuid(), Title = "Глеб" },
            new Name { Id = Guid.NewGuid(), Title = "Гордей" },
            new Name { Id = Guid.NewGuid(), Title = "Гостомысл" },
            new Name { Id = Guid.NewGuid(), Title = "Гремислав" },
            new Name { Id = Guid.NewGuid(), Title = "Григорий" },
            new Name { Id = Guid.NewGuid(), Title = "Гурий" },
            new Name { Id = Guid.NewGuid(), Title = "Давыд" },
            new Name { Id = Guid.NewGuid(), Title = "Данила" },
            new Name { Id = Guid.NewGuid(), Title = "Дементий" },
            new Name { Id = Guid.NewGuid(), Title = "Демид" },
            new Name { Id = Guid.NewGuid(), Title = "Демьян" },
            new Name { Id = Guid.NewGuid(), Title = "Денис" },
            new Name { Id = Guid.NewGuid(), Title = "Дмитрий" },
            new Name { Id = Guid.NewGuid(), Title = "Добромысл" },
            new Name { Id = Guid.NewGuid(), Title = "Доброслав" },
            new Name { Id = Guid.NewGuid(), Title = "Дорофей" },
            new Name { Id = Guid.NewGuid(), Title = "Евгений" },
            new Name { Id = Guid.NewGuid(), Title = "Евграф" },
            new Name { Id = Guid.NewGuid(), Title = "Евдоким" },
            new Name { Id = Guid.NewGuid(), Title = "Евлампий" },
            new Name { Id = Guid.NewGuid(), Title = "Евсей" },
            new Name { Id = Guid.NewGuid(), Title = "Евстафий" },
            new Name { Id = Guid.NewGuid(), Title = "Евстигней" },
            new Name { Id = Guid.NewGuid(), Title = "Егор" },
            new Name { Id = Guid.NewGuid(), Title = "Елизар" },
            new Name { Id = Guid.NewGuid(), Title = "Елисей" },
            new Name { Id = Guid.NewGuid(), Title = "Емельян" },
            new Name { Id = Guid.NewGuid(), Title = "Епифан" },
            new Name { Id = Guid.NewGuid(), Title = "Еремей" },
            new Name { Id = Guid.NewGuid(), Title = "Ермил" },
            new Name { Id = Guid.NewGuid(), Title = "Ермолай" },
            new Name { Id = Guid.NewGuid(), Title = "Ерофей" },
            new Name { Id = Guid.NewGuid(), Title = "Ефим" },
            new Name { Id = Guid.NewGuid(), Title = "Ефрем" },
            new Name { Id = Guid.NewGuid(), Title = "Захар" },
            new Name { Id = Guid.NewGuid(), Title = "Зиновий" },
            new Name { Id = Guid.NewGuid(), Title = "Зосима" },
            new Name { Id = Guid.NewGuid(), Title = "Иван" },
            new Name { Id = Guid.NewGuid(), Title = "Игнатий" },
            new Name { Id = Guid.NewGuid(), Title = "Игорь" },
            new Name { Id = Guid.NewGuid(), Title = "Измаил" },
            new Name { Id = Guid.NewGuid(), Title = "Изот" },
            new Name { Id = Guid.NewGuid(), Title = "Изяслав" },
            new Name { Id = Guid.NewGuid(), Title = "Иларион" },
            new Name { Id = Guid.NewGuid(), Title = "Илья" },
            new Name { Id = Guid.NewGuid(), Title = "Иннокентий" },
            new Name { Id = Guid.NewGuid(), Title = "Иосиф" },
            new Name { Id = Guid.NewGuid(), Title = "Ипат" },
            new Name { Id = Guid.NewGuid(), Title = "Ипатий" },
            new Name { Id = Guid.NewGuid(), Title = "Ипполит" },
            new Name { Id = Guid.NewGuid(), Title = "Ираклий" },
            new Name { Id = Guid.NewGuid(), Title = "Исай" },
            new Name { Id = Guid.NewGuid(), Title = "Исидор" },
            new Name { Id = Guid.NewGuid(), Title = "Казимир" },
            new Name { Id = Guid.NewGuid(), Title = "Каллистрат" },
            new Name { Id = Guid.NewGuid(), Title = "Капитон" },
            new Name { Id = Guid.NewGuid(), Title = "Карл" },
            new Name { Id = Guid.NewGuid(), Title = "Карп" },
            new Name { Id = Guid.NewGuid(), Title = "Касьян" },
            new Name { Id = Guid.NewGuid(), Title = "Ким" },
            new Name { Id = Guid.NewGuid(), Title = "Кир" },
            new Name { Id = Guid.NewGuid(), Title = "Кирилл" },
            new Name { Id = Guid.NewGuid(), Title = "Клавдий" },
            new Name { Id = Guid.NewGuid(), Title = "Климент" },
            new Name { Id = Guid.NewGuid(), Title = "Кондрат" },
            new Name { Id = Guid.NewGuid(), Title = "Кондратий" },
            new Name { Id = Guid.NewGuid(), Title = "Конон" },
            new Name { Id = Guid.NewGuid(), Title = "Константин" },
            new Name { Id = Guid.NewGuid(), Title = "Корнил" },
            new Name { Id = Guid.NewGuid(), Title = "Кузьма" },
            new Name { Id = Guid.NewGuid(), Title = "Куприян" },
            new Name { Id = Guid.NewGuid(), Title = "Лавр" },
            new Name { Id = Guid.NewGuid(), Title = "Лаврентий" },
            new Name { Id = Guid.NewGuid(), Title = "Ладимир" },
            new Name { Id = Guid.NewGuid(), Title = "Ладислав" },
            new Name { Id = Guid.NewGuid(), Title = "Лазарь" },
            new Name { Id = Guid.NewGuid(), Title = "Лев" },
            new Name { Id = Guid.NewGuid(), Title = "Леон" },
            new Name { Id = Guid.NewGuid(), Title = "Леонид" },
            new Name { Id = Guid.NewGuid(), Title = "Леонтий" },
            new Name { Id = Guid.NewGuid(), Title = "Лонгин" }
            );

            #endregion

            #region Programming Languages

            context.Languages.AddOrUpdate(x => x.Title,
                new ProgrammingLanguage()
                {
                    Title = "C#",
                    Id = Guid.NewGuid()
                },
                new ProgrammingLanguage()
                {
                    Title = "Javascript",
                    Id = Guid.NewGuid()
                },
                new ProgrammingLanguage()
                {
                    Title = "PHP",
                    Id = Guid.NewGuid()
                }
            );

            #endregion

            #region Departments

            context.Departments.AddOrUpdate(x => x.Title,
                new Department { Title = "Отдел 1", Floor = 1, Id = Guid.NewGuid() },
                new Department { Title = "Отдел 2", Floor = 2, Id = Guid.NewGuid() }
                );

            #endregion

            base.Seed(context);
        }
    }
}