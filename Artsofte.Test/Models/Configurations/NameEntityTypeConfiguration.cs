﻿using Artsofte.Test.Areas.Team.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Artsofte.Test.Models.Configurations
{
    public class NameEntityTypeConfiguration : EntityTypeConfiguration<Name>
    {
        public NameEntityTypeConfiguration()
        {
            this.HasKey(x => x.Id);
            this.Property(x => x.Title).IsRequired();
        }
    }
}