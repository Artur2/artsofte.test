﻿using Artsofte.Test.Areas.Team.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Artsofte.Test.Models.Configurations
{
    public class DepartmentEntityTypeConfiguration : EntityTypeConfiguration<Department>
    {
        public DepartmentEntityTypeConfiguration()
        {
            this.HasKey(x => x.Id);
            this.Property(x => x.Title).IsRequired();
            this.Property(x => x.Floor).IsRequired();

            this.HasMany(x => x.Employees)
                .WithOptional(x => x.Department)
                .HasForeignKey(x => x.DepartmentId)
                .WillCascadeOnDelete(false);
        }
    }
}