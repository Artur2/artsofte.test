﻿using Artsofte.Test.Areas.Team.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Artsofte.Test.Models.Configurations
{
    public class ProgrammingLanguageEntityTypeConfiguration : EntityTypeConfiguration<ProgrammingLanguage>
    {
        public ProgrammingLanguageEntityTypeConfiguration()
        {
            this.HasKey(x => x.Id);
            this.Property(x => x.Title).IsRequired();

            this.HasMany(x => x.Employees)
                .WithOptional(x => x.Language)
                .HasForeignKey(x => x.LanguageId)
                .WillCascadeOnDelete(false);
        }
    }
}