﻿using Artsofte.Test.Models.Configurations;
using Artsofte.Test.Models.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Artsofte.Test
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DataContext, MigrateToLatestVersionConfiguration>("DefaultConnection"));
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DataContext>());
        }

        protected void Application_EndRequest()
        {
            DataContext.Close();
        }
    }
}
